package foo.concert.member;

import foo.concert.Song.Song;
import foo.concert.instrument.MusicalInstrument;

/**
 * Created by ����� on 17.02.2016.
 */
public class SongMan implements Member {

    Song text=null;

    public SongMan(Song text){
        this.text=text;
    }

    @Override
    public void play() {
        text.song();
    }
}
