package foo.concert.member;

import foo.concert.Song.Song;
import foo.concert.instrument.MusicalInstrument;

/**
 * Created by Роман on 17.02.2016.
 */
public class GuitarMan implements Member {

    MusicalInstrument instrument = null;

    Song text=null;

    public GuitarMan(MusicalInstrument instrument){
        this.instrument=instrument;
    }

    public GuitarMan(MusicalInstrument instrument, Song song){
        this.instrument=instrument;
        this.text=song;
    }

    @Override
    public void play() {
        System.out.println(instrument.sound());
        if(text!=null){
            text.song();
        }
    }
}
