package foo.concert.member;

import foo.concert.Song.Song;
import foo.concert.instrument.MusicalInstrument;

/**
 * Created by ����� on 17.02.2016.
 */
public class BalalaikaMan implements Member {

    MusicalInstrument instrument = null;

    Song text=null;

    public BalalaikaMan(MusicalInstrument instrument){
        this.instrument=instrument;
    }

    public BalalaikaMan(MusicalInstrument instrument, Song song){
        this.instrument=instrument;
        this.text=song;
    }

    @Override
    public void play() {
        System.out.println(instrument.sound());
        if(text!=null){
            text.song();
        }
    }
}
