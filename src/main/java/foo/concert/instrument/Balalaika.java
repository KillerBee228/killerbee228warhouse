package foo.concert.instrument;

/**
 * Created by ����� on 17.02.2016.
 */
public class Balalaika implements MusicalInstrument {

    String text;

    public Balalaika(String text){
        this.text=text;
    }

    @Override
    public String sound() {
        return text;
    }
}
