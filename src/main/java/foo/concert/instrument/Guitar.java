package foo.concert.instrument;

/**
 * Created by ����� on 17.02.2016.
 */
public class Guitar implements MusicalInstrument {

    String text;

    public Guitar(String text){
        this.text=text;
    }

    @Override
    public String sound() {
        return text;
    }
}
