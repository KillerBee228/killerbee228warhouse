package foo.concert;
import foo.concert.conferance.Conferance;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class StartConcert {
    public static void main(String[] args) {
        /*
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        Conferance conferance = (Conferance) context.getBean("conferance");
        conferance.start();
        */
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(AppConfiguration.class);

        Conferance conferance = ctx.getBean(Conferance.class);
        conferance.start();
    }
}
