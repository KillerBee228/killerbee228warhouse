package foo.concert.collective;

import com.sun.org.apache.xpath.internal.SourceTree;
import foo.concert.Song.Song;
import foo.concert.member.Member;

import java.util.Collection;
import java.util.List;

/**
 * Created by ����� on 17.02.2016.
 */
public class RockGroup implements Collectives {

    Collection <Member> members;

    Collection <Song> songs;

    public RockGroup(){
    }

    public RockGroup(Collection <Member> members, Collection <Song> songs){
        this.members=members;
        this.songs=songs;
    }



    @Override
    public void comeOut() {
        System.out.println("RockGroup is here and our first song:");
        for(Song song : songs){
            song.whichsong();
            song.song();
            for(Member member : members){
                member.play();
            }
            song.song();
        }

    }

    public void setMembers(Collection members) {
        this.members = members;
    }

    public Collection getMembers() {
        return members;
    }

    public Collection getSongs() {
        return songs;
    }

    public void setSongs(Collection songs) {
        this.songs = songs;
    }
}
