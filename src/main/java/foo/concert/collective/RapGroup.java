package foo.concert.collective;

import foo.concert.Song.Song;
import foo.concert.member.Member;

import java.util.Collection;

/**
 * Created by ����� on 18.02.2016.
 */
public class RapGroup implements Collectives {

    Collection <Member> members;

    Collection <Song> songs;

    public RapGroup() {
    }

    public RapGroup(Collection <Member> members, Collection <Song> songs){
        this.members=members;
        this.songs=songs;
    }

    @Override
    public void comeOut() {
        System.out.println("RapGroup is here and our first song:");
        for(Song song : songs){
            song.whichsong();
            song.song();
            for(Member member : members){
                member.play();
            }
            song.song();
        }

    }

    public void setMembers(Collection members) {
        this.members = members;
    }

    public Collection getMembers() {
        return members;
    }

    public void setSongs(Collection songs) {
        this.songs = songs;
    }

    public Collection getSongs() {
        return songs;
    }
}
