package foo.concert.Song;

/**
 * Created by ����� on 17.02.2016.
 */
public class SongBalalaikaMan implements Song {

    String namesong="Song under balalaika";

    String text="Sing under balalaika";

    public SongBalalaikaMan(){

    }

    public SongBalalaikaMan(String text,String namesong){
        this.namesong=namesong;
        this.text=text;
    }

    @Override
    public void song() {
        System.out.println(text);
    }

    @Override
    public void whichsong() {
        System.out.println("Now play:"+namesong);
    }
}
