package foo.concert.Song;

/**
 * Created by ����� on 17.02.2016.
 */
public class SongBassGuitarMan implements Song {

    String namesong="Song under bassguitar";

    String text="Sing under bassguitar";

    public SongBassGuitarMan(){
    }

    public SongBassGuitarMan(String text, String namesong){
        this.namesong=namesong;
        this.text=text;
    }

    @Override
    public void song() {
        System.out.println(text);
    }

    @Override
    public void whichsong() {
        System.out.println("Now play:"+namesong);
    }
}
