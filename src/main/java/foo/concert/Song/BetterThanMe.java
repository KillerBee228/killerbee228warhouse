package foo.concert.Song;

public class BetterThanMe implements Song {

    String namesong="Song Betterthanme";

    String text="Sing Betterthanme";

    public BetterThanMe(){

    }

    public BetterThanMe(String text,String namesong){
        this.namesong=namesong;
        this.text=text;
    }

    @Override
    public void song() {
        System.out.println(text);
    }

    @Override
    public void whichsong() {
        System.out.println("Now play:"+namesong);
    }
}
