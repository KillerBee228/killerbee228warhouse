package foo.concert;

import foo.concert.Song.*;
import foo.concert.collective.Collectives;
import foo.concert.collective.RapGroup;
import foo.concert.collective.RockGroup;
import foo.concert.conferance.Conferance;
import foo.concert.instrument.Balalaika;
import foo.concert.instrument.Barabans;
import foo.concert.instrument.BassGuitar;
import foo.concert.instrument.Guitar;
import foo.concert.member.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collection;

@Configuration
public class AppConfiguration {

    @Bean
    public Balalaika Balalaika(){
        return new Balalaika("Play balalaika");
    }

    @Bean
    public Barabans Barabans(){
        return new Barabans("Play Barabans");
    }

    @Bean
    public BassGuitar BassGuitar(){
        return new BassGuitar("Play Bassguitar");
    }

    @Bean
    public Guitar Guitar(){
        return new Guitar("Play Guitar");
    }

    @Bean
    public BetterThanMe BetterThanMe(){
        return new BetterThanMe();
    }

    @Bean
    public Brothers Brothers(){
        return new Brothers();
    }

    @Bean
    public Overmyhead Overmyhead(){
        return new Overmyhead();
    }

    @Bean
    public SongBalalaikaMan SongBalalaikaMan(){
        return new SongBalalaikaMan();
    }

    @Bean
    public SongBassGuitarMan SongBassGuitar(){
        return new SongBassGuitarMan();
    }

    @Bean
    public RapGroup RapGroup(){
        Collection<Member> members = new ArrayList<Member>();
        Collection<Song> songs = new ArrayList<Song>();
        members.add(new SongMan(Brothers()));
        members.add(new BalalaikaMan(Balalaika()));
        members.add(new GuitarMan(Guitar()));
        songs.add(Brothers());
        songs.add(BetterThanMe());
        return new RapGroup(members,songs);
    }

    @Bean
    public RockGroup RockGroup(){
        Collection<Member> members = new ArrayList<Member>();
        Collection<Song> songs = new ArrayList<Song>();
        members.add(new BassGuitarMan(BassGuitar(), Overmyhead()));
        members.add(new BarabansMan(Barabans()));
        members.add(new GuitarMan(Guitar()));
        songs.add(Overmyhead());
        songs.add(SongBassGuitar());
        return new RockGroup(members,songs);
    }

    @Bean
    public Conferance Conferance(){
        Collection<Collectives> collectives= new ArrayList<Collectives>();
        collectives.add(RapGroup());
        collectives.add(RockGroup());
        return new Conferance(collectives);
    }
}
