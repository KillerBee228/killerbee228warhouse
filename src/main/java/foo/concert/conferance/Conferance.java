package foo.concert.conferance;

import foo.concert.collective.Collectives;

import java.util.Collection;
import java.util.List;

/**
 * Created by ����� on 19.02.2016.
 */
public class Conferance {

    Collection <Collectives> collectives;

    public Conferance(){

    }

    public Conferance(Collection <Collectives> collectives){
        this.collectives=collectives;
    }

    public void start(){
        System.out.println("Concert is start");
        for(Collectives collective : collectives){
            collective.comeOut();
        }
    }

    public void setCollectives(List collectives) {
        this.collectives = collectives;
    }

    public Collection getCollectives() {
        return collectives;
    }
}
