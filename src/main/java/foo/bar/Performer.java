package foo.bar;

import java.util.Collections;

/**
 * Created by etc on 20.03.2015.
 */
public interface Performer {
    void perform();
}
