package foo.bar;

import java.util.Collection;

/**
 * Created by ����� on 10.02.2016.
 */
public class Conferance  {
    Collection<Performer> actors;

    public Collection<Performer> getActors() {
        return actors;
    }

    public void setActors(Collection<Performer> actors) {
        this.actors = actors;
    }

    public void perform() {
        for(Performer performer1:actors) {
            performer1.perform();
        }
    }
}
