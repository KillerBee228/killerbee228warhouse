package foo.concert.instrument;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-config.xml")

public class BassGuitarTests {
    @Autowired
    BassGuitar bassguitar;

    @Test
    public void soundTest(){
        Assert.assertEquals("must be 'Play bassguitar'","Play bassguitar",bassguitar.sound());
    }
}
